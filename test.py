def HelloWorld():
    print("Hello World")

def HelloWorld2():
    print("Hello World 2")

class Personne:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def set_name(self, name):
        self.name = name

    def set_age(self, age):
        self.age = age

    def display(self):
        print("Name: ", self.name)
        print("Age: ", self.age)

class Test:
    def __init__(self):
        self.person = Personne("John", 25)

    def display(self):
        self.person.display()

    def set_name(self, name):
        self.person.set_name(name)

    def set_age(self, age):
        self.person.set_age(age)

    def get_name(self):
        return self.person.get_name()

    def get_age(self):
        return self.person.get_age()

    def get_person(self):
        return self.person

    def set_person(self, person):
        self.person = person

    def display_person(self):
        self.person.display()

    def display_person_name(self):
        print("Name: ", self.person.get_name())

    def display_person_age(self):
        print("Age: ", self.person.get_age())

    def display_person_name_age(self):
        print("Name: ", self.person.get_name())
        print("Age: ", self.person.get_age())